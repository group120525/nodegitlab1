
// index.js

// Import the express module
const express = require('express');

// Create an instance of express
const app = express();

// Define a port to listen on
const port = 3001;

// Define a route handler for the root URL (/)
app.get('/', (req, res) => {
  res.send('Hello, World!');
});

// Start the server and listen on the specified port
app.listen(port, () => {
  console.log(`Express app listening at http://localhost:${port}`);
});
